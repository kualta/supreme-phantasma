﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGUI : MonoBehaviour
{
    public Texture2D crosshairTexture;
    public Rect crosshairPosition;

    void Start()
    {
        crosshairPosition = new Rect((Screen.width - crosshairTexture.width) / 2,
                                     (Screen.height - crosshairTexture.height) /2,
                                     crosshairTexture.width, crosshairTexture.height);
    }

    void OnGUI() {
        GUI.DrawTexture(crosshairPosition, crosshairTexture);
    }
}
