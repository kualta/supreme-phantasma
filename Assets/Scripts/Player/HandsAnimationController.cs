﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsAnimationController : MonoBehaviour
{
    [SerializeField]
    internal Animator animation;


    void OnShootingEnd() {
        animation.SetBool("Shooting", false);
    }
}
