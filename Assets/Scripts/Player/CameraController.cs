﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    internal PlayerController controller;

    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;

    public GameObject cameraObj;
    public GameObject character;

    private Vector2 mouseLook;
    private Vector2 smoothV;


    void Start () {

        // GameObject parentObject = NetworkIdentity.spawned[parentNetId].gameObject;
        // transform.SetParent(parentObject.transform);

        character = this.gameObject;
        cameraObj = controller.cameraObj;

        if ( controller.hasAuthority ) {
            cameraObj.GetComponent<Camera>().enabled = true;
        }
    }


    void Update () {

        Debug.DrawRay(cameraObj.transform.position, cameraObj.transform.forward, Color.red);

        if ( !controller.hasAuthority ) {
            return;
        }

        Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        mouseDelta = Vector2.Scale(mouseDelta, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

        // the interpolated float result between the two float values
        smoothV.x = Mathf.Lerp(smoothV.x, mouseDelta.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, mouseDelta.y, 1f / smoothing);

        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

        // vector3.right means the x-axis
        cameraObj.transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
    }
}
