﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerController : NetworkBehaviour
{
    [SerializeField]
    internal CameraController camera;

    [SerializeField]
    internal InputController input;

    [SerializeField]
    internal MovementController movement;

    [SerializeField]
    internal WeaponController weapon;

    public AnimationController animation;
    public Animator handsAnimation;

    public PlayerConnectionController connectionController;

    [SyncVar]
    public GameObject connectionControllerObj;

    public GameObject cameraObj;
    public GameObject model;
    public GameObject firstPersonHandsObj;

    public void SetConnectionController(PlayerConnectionController c) {
       connectionController = c;
    }

    public void SetConnectionControllerObj(GameObject controllerObj) {
        connectionControllerObj = controllerObj;
    }

    public void OnDeath(string killer) {
        connectionController.OnDeath(this.gameObject, killer);
        Debug.Log("PlayerController::OnDeath()");
    }

    void Start() {
        animation = model.GetComponent<AnimationController>();
        handsAnimation = firstPersonHandsObj.GetComponent<Animator>();

        if ( hasAuthority ) {
            model.SetActive(false);
        } else {
            firstPersonHandsObj.SetActive(false);
        }

        SetConnectionController(connectionControllerObj.GetComponent<PlayerConnectionController>());

        input = connectionControllerObj.GetComponent<InputController>();

        this.gameObject.name = connectionController.playerNickname + "'s Unit";
    }
}
