﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class InputController : NetworkBehaviour
{
    public float inputHorizontal;
    public float inputVertical;

    public bool jumpPressed;

    [SyncVar]
    public GameObject controllerObj;

    internal PlayerController controller;
    internal NetworkManager manager;
    internal ChatServer chatServer;

    internal void SetPlayerController(PlayerController c) {
        controller = c;
    }

    public void SetPlayerControllerObj(GameObject obj) {

        controllerObj = obj;

        // FIXME: This function doest two different things instead of one due to bug
        // when controllerObj being uninitialized when script starts, that's why next
        // line fails if put in Start(). Too bad!

        RpcSetPC(controllerObj);
    }

    [ClientRpc]
    void RpcSetPC(GameObject obj) {
        SetPlayerController(obj.GetComponent<PlayerController>());
    }

    public GameObject GetPlayerControllerObj() {
        return controllerObj;
    }

    void Start()
    {
        manager = NetworkManager.singleton;
        Cursor.lockState = CursorLockMode.Locked;
        chatServer = this.gameObject.GetComponent<ChatServer>();
    }

    void Update()
    {
        if ( !hasAuthority ) {
            return;
        }

        GetInput();
    }

    void OnJump() {
        jumpPressed = true;
        controller.movement.OnJump();
    }

    void OnLand() {
        jumpPressed = false;
    }

    void OnSprint() {
        controller.movement.OnSprint();
        // controller.animation.OnSprint();
    }

    void OnWalk() {
        controller.movement.OnWalk();
        // controller.animation.OnWalk();
    }

    void OnIdle() {
        // controller.animation.OnIdle();
    }

    void OnFire() {
        controller.weapon.OnFire();
        controller.handsAnimation.SetBool("Shooting", true);
    }

    void GetInput() {

        if (Input.GetButtonUp("Jump")) {
            OnLand();
        }

        if (Input.GetButtonDown("Jump")) {
            OnJump();
        }

        if (Input.GetButtonDown("Sprint")) {
            OnSprint();
        }

        if (Input.GetButtonUp("Sprint")) {
            OnWalk();
        }

        if (Input.GetButtonDown("Fire")) {
            Cursor.lockState = CursorLockMode.Locked;
            OnFire();
        }

        if (Input.GetButtonDown("ChatSubmit")) {

            if (!chatServer.inputBox.isFocused) {
                chatServer.inputBox.ActivateInputField();
            }

            chatServer.OnChatInputSubmit();
        }

        inputHorizontal = Input.GetAxis("Horizontal");
        inputVertical = Input.GetAxis("Vertical");

        if ( inputHorizontal == 0 && inputVertical == 0 ) {
            OnIdle();
        } else if ( (inputHorizontal != 0 || inputVertical != 0) && !Input.GetButton("Sprint") ) {
            OnWalk();
        }

        if (Input.GetKeyDown(KeyCode.Escape) == true && Cursor.lockState == CursorLockMode.Locked) {

            Cursor.lockState = CursorLockMode.None;

        } else if (Input.GetKeyDown(KeyCode.Escape) == true && Cursor.lockState == CursorLockMode.None) {

            if (NetworkServer.active && NetworkClient.isConnected)
            {
                manager.StopHost();
            }
            else if (NetworkClient.isConnected)
            {
                manager.StopClient();
            }
            else if (NetworkServer.active)
            {
                manager.StopServer();
            }
        }
    }
}
