﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInformation : MonoBehaviour
{
    public string playerNickname;

    public void ChangeNickname(string newNickname) {
        playerNickname = newNickname;
    }

    public string GetNickname() {
        return playerNickname;
    }
}
