﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BulletController : NetworkBehaviour
{
    public Rigidbody rigidBody;

    public WeaponController parentController;

    [SyncVar]
    public GameObject parentObj;

    [SyncVar]
    public Vector3 direction;

    public float bulletSpeed = 15f;
    public float bulletLifeSpan = 12f;

    public int bouncesAmount = 0;
    public int maxBouncesAmount = 5;


    public void SetDirection(Vector3 newDirection) {
        direction = newDirection;
    }

    public void SetOwner(WeaponController owner) {
        parentController = owner;
    }

    public void SetOwnerObj(GameObject owner) {
        parentObj = owner;
    }

    void Start() {
        SetOwner(parentObj.GetComponent<WeaponController>());

        if ( !hasAuthority ) {
            return;
        }

        StartCoroutine(DestroyBulletTimer());
        rigidBody = GetComponent<Rigidbody>();
    }

    void Update() {

        if ( !hasAuthority ) {
            return;
        }

        // rigidBody.AddForce(direction.normalized * bulletSpeed, ForceMode.VelocityChange);
        // CmdMoveBullet();
        rigidBody.velocity = direction.normalized * bulletSpeed;

        if (bouncesAmount >= maxBouncesAmount) {
            CmdDestroyBullet();
        }
    }


    [Command]
    void CmdMoveBullet() {
        // charController.Move(direction.normalized * bulletSpeed * Time.deltaTime);
        // rigidBody.AddForce(direction.normalized * bulletSpeed, ForceMode.VelocityChange);
    }

    [Command]
    void CmdDestroyBullet() {
        NetworkServer.Destroy(transform.gameObject);
    }

    void OnCollisionEnter(Collision collision) {
        if ( collision.gameObject.tag == "Player") {
            if ( collision.gameObject.GetComponent<PlayerController>().hasAuthority ) {
                return;
            }
        }
        Debug.Log("CollisionEnter BulletController");
        SetDirection(Vector3.Reflect(direction, collision.GetContact(0).normal.normalized));
        bouncesAmount++;
    }

    // void OnControllerColliderHit(ControllerColliderHit hit) {
    //     SetDirection(Vector3.Reflect(hit.moveDirection.normalized, hit.normal.normalized));
    //     bouncesAmount++;
    // }

    void OnDestroy() {
        parentController.OnBulletDestroy();
    }

    IEnumerator DestroyBulletTimer() {
        yield return new WaitForSeconds(bulletLifeSpan);
        CmdDestroyBullet();
    }
}
