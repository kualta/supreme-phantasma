﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MovementController : NetworkBehaviour
{
    [SerializeField]
    internal PlayerController controller;


    public float walkSpeed = 6f;
    public float runSpeed = 10f;
    public float jumpSpeed = 8f;
    public float jumpAcceleration = 2f;
    public float fallSpeed = 6f;
    public float currentFallSpeed = 0f;
    public float jumpTime = 2f;
    public Vector3 jumpVector;

    public ForceMode jumpForceMode;
    public ForceMode fallForceMode;
    public ForceMode moveForceMode;

    public LayerMask layerMask;

    public Vector3 moveDirection = Vector3.zero;

    internal Rigidbody rigidBody;
    internal float speed;
    internal CapsuleCollider characterCollider;
    internal bool isGrounded;
    internal bool stairForward;


    public void OnJump() {
        if ( isGrounded ) {
            StartCoroutine(Jump());
        }
    }


    public void OnSprint() {
        speed = runSpeed;
    }

    public void OnWalk() {
        speed = walkSpeed;
    }

    [ClientRpc]
    void RpcAnimUpdate() {
        if (moveDirection == Vector3.zero) {
            controller.animation.OnIdle();
        } else if (speed == walkSpeed) {
            controller.animation.OnWalk();
        } else if (speed == runSpeed){
            controller.animation.OnSprint();
        }
    }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        characterCollider = GetComponent<CapsuleCollider>();
        OnWalk();
    }

    // void Update() {
    //     if ( !hasAuthority ) {

    //         RpcAnimUpdate();

    //         return;
    //     }
    // }

    void FixedUpdate()
    {
        // NOTE: Update() runs on EVERY client

        HandleGround();

        if ( !hasAuthority ) {

            return;
        }

        moveDirection = Vector3.zero;
        Vector3 userInput = controller.input.inputHorizontal * transform.right + controller.input.inputVertical * transform.forward;
        moveDirection += Vector3.ClampMagnitude(userInput, 1f);
        moveDirection *= speed;

        if ( isGrounded ) {
            currentFallSpeed = 0f;
        } else {
            currentFallSpeed += Physics.gravity.y * fallSpeed;
            rigidBody.AddForce(Vector3.up * currentFallSpeed, fallForceMode);
        }

        if ( stairForward ) {
            rigidBody.AddForce(Vector3.up * 200f, jumpForceMode);
            rigidBody.AddForce(moveDirection * 100f, jumpForceMode);
        }


        // rigidBody.AddForce(moveDirection, moveForceMode);
        rigidBody.velocity = moveDirection;
    }

    void HandleGround() {
        CheckForStairs();
        CheckForGround();
    }

    void CheckForGround() {
        float distanceToGround = characterCollider.bounds.extents.y;

        Debug.DrawRay(transform.position + Vector3.up, Vector3.down * (distanceToGround + 0.1f));

        isGrounded = Physics.Raycast(transform.position + Vector3.up, Vector3.down, distanceToGround + 0.1f);
    }

    void CheckForStairs() {
        Vector3 rayOrigin = transform.position + moveDirection.normalized * 0.6f + Vector3.up * 0.8f;
        Vector3 rayDirection = Vector3.down;
        float rayLenght = 0.6f;

        if (moveDirection != Vector3.zero) {
            stairForward = Physics.Raycast(rayOrigin, rayDirection, rayLenght, layerMask);
        } else {
            stairForward = false;
        }

        if ( stairForward ) {
            Debug.DrawRay(rayOrigin, rayDirection * rayLenght, Color.red);
        }
    }

    [ServerCallback]
    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Bullet") {

            Debug.Log("MovementController::OnCollisionEnter" + " This: " + controller.weapon + " Collision: "
                      + collision.gameObject.GetComponent<BulletController>().parentController);
            if (collision.gameObject.GetComponent<BulletController>().parentController == GetComponent<WeaponController>()) {
                Debug.Log("Same parent controller, quiting");
                return;
            }

            controller.OnDeath(collision.gameObject.GetComponent<BulletController>().parentController.controller.connectionController.playerNickname);
        }
    }


    IEnumerator Jump() {
        rigidBody.velocity = Vector3.zero;
        float timer = 0.1f;

        Debug.Log(controller.input.jumpPressed);
        while(controller.input.jumpPressed && timer < jumpTime)
        {
            float proportionCompleted = timer / jumpTime;
            Vector3 currentJumpVector = Vector3.Lerp(jumpVector, Vector3.zero, proportionCompleted);
            rigidBody.AddForce(currentJumpVector);
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
