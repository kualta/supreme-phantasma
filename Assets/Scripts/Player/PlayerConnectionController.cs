﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerConnectionController : NetworkBehaviour
{
    public GameObject PlayerUnit;
    public GameObject PlayerCamera;
    public GameObject chatLayer;
    public Transform[] spawnPoints;

    public Camera lobbyCamera;

    [SyncVar]
    public bool isPlayerAlive;

    [SyncVar]
    public string playerNickname;

    void Start()
    {
        CmdSetPlayerNickname(NetworkManager.singleton.GetComponent<PlayerInformation>().GetNickname());

        if (playerNickname != "") {
            this.gameObject.name = playerNickname + "'s ConnectionController";
        }

        spawnPoints = GameObject.Find("SpawnPoints").GetComponent<SpawnPointsLibrary>().spawnPointsLibrary;
        OnSpawn();
        isPlayerAlive = true;
    }

    void Update()
    {
        // NOTE: Update() runs on EVERYONE's computer. Hurrah for validations!

        if ( !isLocalPlayer ) {
            return;
        }


        if ( !isPlayerAlive ) {
            StartCoroutine(RespawnDelay());
        }
    }

    public void OnSpawn() {
        if ( !isLocalPlayer ) {
            return;
        }

        CmdSpawnPlayer();
    }

    public void OnDeath(GameObject playerUnit, string killer) {
        if ( !isLocalPlayer ) {
            return;
        }

        CmdKillPlayer(playerUnit, killer);
        isPlayerAlive = false;
    }


    [Command]
    void CmdSetPlayerNickname(string nickname) {
        playerNickname = nickname;
    }

    [Command]
    void CmdSpawnPlayer() {
        GameObject playerObj = Instantiate(PlayerUnit);
        playerObj.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;

        PlayerController pc = playerObj.GetComponent<PlayerController>();
        pc.SetConnectionControllerObj(this.gameObject);

        lobbyCamera = GameObject.Find("Lobby Camera").GetComponent<Camera>();
        lobbyCamera.enabled = false;

        NetworkServer.Spawn(playerObj, connectionToClient);
        this.gameObject.GetComponent<InputController>().SetPlayerControllerObj(playerObj);
    }

    [Command]
    public void CmdKillPlayer(GameObject playerUnit, string killer) {
        SpawnKillfeedEntry(killer);
        NetworkServer.Destroy(playerUnit);

        lobbyCamera = GameObject.Find("Lobby Camera").GetComponent<Camera>();
        lobbyCamera.enabled = true;
    }

    internal void SpawnKillfeedEntry(string killer) {
        this.GetComponent<KillfeedServer>().OnKill(killer);
    }

    IEnumerator RespawnDelay() {
        isPlayerAlive = true;
        yield return new WaitForSeconds(3f);

        OnSpawn();
    }
}
