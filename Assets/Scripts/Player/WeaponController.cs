﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class WeaponController : NetworkBehaviour
{
    [SerializeField]
    internal PlayerController controller;

    public GameObject bulletPrefab;
    public GameObject[] bulletsList;
    public Transform bulletSpawnPoint;

    public int maxBulletAmount;
    [SyncVar]
    public int currentBulletAmount;


    public void OnFire() {
        if (currentBulletAmount < maxBulletAmount) {
            CmdPrintValue(this.netId);
            CmdSpawnBullet(controller.camera.cameraObj.transform.forward, bulletSpawnPoint.position);
        }
    }

    public void OnBulletDestroy() {
        CmdReduceCurrentBulletAmount();
    }

    void Start()
    {
        currentBulletAmount = 0;
        maxBulletAmount = 5;
    }


    [Command]
    void CmdSpawnBullet(Vector3 direction, Vector3 spawnPoint) {
        GameObject bullet = Instantiate(bulletPrefab, spawnPoint, Quaternion.identity);

        bullet.GetComponent<BulletController>().SetDirection(direction);
        bullet.GetComponent<BulletController>().SetOwnerObj(this.gameObject);
        bullet.tag = "Bullet";

        // RpcConfigureBullet(bullet);

        NetworkServer.Spawn(bullet, connectionToClient);
        currentBulletAmount++;
    }

    [Command]
    void CmdReduceCurrentBulletAmount() {
        currentBulletAmount--;
    }

    // [ClientRpc]
    // void RpcConfigureBullet(GameObject bullet) {
    //     // bullet.GetComponent<BulletController>().SetDirection(direction);
    //     bullet.GetComponent<BulletController>().SetOwner(this);
    //     bullet.tag = "Bullet";
    // }

    [Command]
    void CmdPrintValue(uint value) {
        Debug.Log("Value on server: " + value);
    }
}
