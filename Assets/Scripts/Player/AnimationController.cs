﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class AnimationController : NetworkBehaviour
{
    public RuntimeAnimatorController[] animatorControllers;
    public Animator animator;
    public PlayerController controller;

    public void OnIdle() {
        animator.runtimeAnimatorController = animatorControllers[0];
    }

    public void OnWalk() {
        animator.runtimeAnimatorController = animatorControllers[1];
    }

    public void OnSprint() {
        animator.runtimeAnimatorController = animatorControllers[2];
    }

    void Start() {
        controller = transform.parent.gameObject.GetComponent<PlayerController>();
    }
}
