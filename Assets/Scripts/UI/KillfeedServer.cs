﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class KillfeedServer : NetworkBehaviour
{

    public GameObject KFLayer;

    public Color AllyColor, EnemyColor;

    public Killfeed clientFeed;


    public void OnKill(string secondPlayer) {
        CmdSendMessage(NetworkManager.singleton.GetComponent<PlayerInformation>().GetNickname(), secondPlayer, FeedEntry.EntryType.kill);
    }

    [Command]
    public void CmdSendMessage(string playerOne, string playerTwo, FeedEntry.EntryType entryType) {
        RpcSendMessage(playerOne, playerTwo, entryType);
    }

    [ClientRpc]
    public void RpcSendMessage(string playerOne, string playerTwo, FeedEntry.EntryType entryType) {

        // FIXME/HACK: This next line is a terrible hack, whole system needs to be
        // fixed to avoid it.
        clientFeed = GameObject.Find("KFController").GetComponent<Killfeed>();

        if (clientFeed.killList.Count >= clientFeed.maxMessagesAmount) {
            Destroy(clientFeed.killList[0].panelObject);
            clientFeed.killList.Remove(clientFeed.killList[0]);
        }

        FeedEntry newFeedEntry = new FeedEntry();

        newFeedEntry.playerOne = playerOne;
        newFeedEntry.playerTwo = playerTwo;

        GameObject newFeedObject = Instantiate(clientFeed.entryObject, clientFeed.KFContent.transform);

        newFeedEntry.panelObject = newFeedObject;

        newFeedEntry.playerOneText = newFeedObject.transform.Find("PlayerOneText").GetComponent<Text>();
        newFeedEntry.playerTwoText = newFeedObject.transform.Find("PlayerTwoText").GetComponent<Text>();

        newFeedEntry.playerOneText.text = newFeedEntry.playerOne;
        newFeedEntry.playerTwoText.text = newFeedEntry.playerTwo;

        clientFeed.killList.Add(newFeedEntry);
    }


    void Start() {

        if ( !hasAuthority ) {
            return;
        }

        clientFeed = Instantiate(KFLayer).GetComponent<Killfeed>();
    }
}

[System.Serializable]
public class FeedEntry {
    public GameObject panelObject;

    public Text playerOneText;
    public Text playerTwoText;

    public string playerOne;
    public string playerTwo;

    public EntryType entryType;

    public enum EntryType {
        kill,
        death
    }
}
