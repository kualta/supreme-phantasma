﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chat : MonoBehaviour
{
    public int maxMessagesAmount = 50;

    public GameObject textObject;
    public GameObject chatContent;

    [SerializeField]
    public List<Message> messageList = new List<Message>();

    void Start() {
        chatContent = GameObject.Find("ChatContent");
    }
}
