﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killfeed : MonoBehaviour
{
    public int maxMessagesAmount = 50;

    public GameObject entryObject;
    public GameObject KFContent;

    [SerializeField]
    public List<FeedEntry> killList = new List<FeedEntry>();

    void Start() {
        KFContent = GameObject.Find("KFContent");
    }
}
