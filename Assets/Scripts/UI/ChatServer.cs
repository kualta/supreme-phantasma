﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class ChatServer : NetworkBehaviour
{

    public GameObject chatLayer;
    public InputField inputBox;

    public Color playerMessageColor, warningColor, infoColor;
    public string warningPrefix, infoPrefix;

    public Chat clientChat;



    public void OnChatInputSubmit() {
        if (inputBox.text == "")
            return;

        CmdSendMessage(inputBox.text, Message.MessageType.playerMessage, NetworkManager.singleton.GetComponent<PlayerInformation>().GetNickname());
        inputBox.text = "";

        inputBox.DeactivateInputField();
    }

    [Command]
    public void CmdSendMessage(string text, Message.MessageType messageType, string sender) {
        RpcSendMessage(text, messageType, sender);
    }

    [ClientRpc]
    public void RpcSendMessage(string text, Message.MessageType messageType, string sender) {

        // FIXME/HACK: This next line is a terrible hack, whole system needs to be
        // fixed to avoid it.
        clientChat = GameObject.Find("ChatController").GetComponent<Chat>();

        if (clientChat.messageList.Count >= clientChat.maxMessagesAmount) {
            Destroy(clientChat.messageList[0].textObject.gameObject);
            clientChat.messageList.Remove(clientChat.messageList[0]);
        }

        Message newMessage = new Message();

        newMessage.text = text;

        GameObject newText = Instantiate(clientChat.textObject, clientChat.chatContent.transform);

        string prefix = MessagePrefix(messageType, sender);

        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = prefix + newMessage.text;
        // newMessage.textObject.color = MessageTypeColor(messageType);

        clientChat.messageList.Add(newMessage);
    }


    void Start() {

        if ( !hasAuthority ) {
            return;
        }

        clientChat = Instantiate(chatLayer).GetComponent<Chat>();
        inputBox = GameObject.Find("ChatInput").GetComponent<InputField>();
    }


    internal string MessagePrefix(Message.MessageType messageType, string sender) {
        string prefix = "<color=lightblue>" + sender + "</color>" + ": ";

        switch(messageType) {
            case Message.MessageType.warning:
                prefix = "<color=red>[Warning]</color> ";
                break;
            case Message.MessageType.info:
                prefix = "<color=blue>[Server]</color> ";
                break;
        }

        return prefix;
    }

    // TODO: Chat colors doesn't work like that. This function is never used.
    internal Color MessageTypeColor(Message.MessageType messageType) {
        Color color = playerMessageColor;

        switch(messageType) {
            case Message.MessageType.warning:
                color = warningColor;
                break;

            case Message.MessageType.info:
                color = infoColor;
                break;
        }

        return color;
    }
}

[System.Serializable]
public class Message
{
    public string text;
    public Text textObject;
    public MessageType messageType;

    public enum MessageType {
        playerMessage,
        warning,
        info
    }
}
