﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MainMenuController : MonoBehaviour
{
    public NetworkManager manager;
    public GameObject managerObj;


    void Awake() {
        managerObj = GameObject.Find("NetworkManager");
        manager = managerObj.GetComponent<NetworkManager>();
    }

    public void OnNickChange(string nick) {
        managerObj.GetComponent<PlayerInformation>().ChangeNickname(nick);
    }

    public void OnHostButton() {
        manager.StartHost();
    }

    public void OnServerOnlyButton() {
        manager.StartServer();
    }

    public void OnConnectDevButton() {
        manager.networkAddress = "d.1xxx.ru";
        manager.StartClient();
    }

    public void OnConnectLocalButton() {
        manager.networkAddress = "localhost";
        manager.StartClient();
    }

    public void OnQuitButton() {
        Application.Quit();
    }
}
